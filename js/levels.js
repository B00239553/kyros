/**
 *   Author: B00239553
 */

/**
 * Class that can be inherited by other levels, all levels will be inherited version of Levels.
 * Class allows other levels to be created without interfering with any other levels created.
 * @param game - phaser game object.
 * @param player - player object.
 * @param level - level object created, is group from phaser engine.
 * @constructor
 */
function Levels(game, player, level)
{
    this.game = game;
    this.player = player;
    this.level = level;
    this.completed = false;    //Used to know if level completed.
    this.levelObjects = null; //Used to hold all level GameObjects.
    this.healthText = null;   //Used to hold player health value and display to screen.
    this.goldText = null;     //Used to hold player gold value and display to screen.
}

/**
 * Function to turn on a switch and change its image to on,
 * will also open the door the switch is connected to.
 * @param Switch - switch to change.
 * @param Door - door to open.
 */
Levels.prototype.flickSwitchDoor = function(Switch, Door)
{
    Switch.on = false;
    Switch.gameElement.kill();
    Switch.gameElement = this.level.create(Switch.x, Switch.y, Switch.endImage);
    Switch.gameElement.body.immovable = true;
    this.openDoor(Door);
}

/**
 * Function to turn on a switch and change its image to on.
 * @param Switch - switch to change.
 */
Levels.prototype.flickSwitch = function(Switch)
{
    Switch.on = true;
    Switch.gameElement.kill();
    Switch.gameElement = this.level.create(Switch.x, Switch.y, Switch.endImage);
    Switch.gameElement.body.immovable = true;
}

/**
 * Function to open switch and change its image to open.
 * This function will also add gold to the player and be displayed on screen.
 * If the player's health is below 100 it will only give half of the gold but
 * will heal the player.
 * @param Chest - chest to change.
 * @param player - player object.
 */
Levels.prototype.openChest = function(Chest, player)
{
    Chest.open = true;
    Chest.gameElement.kill();
    Chest.gameElement = this.level.create(Chest.x, Chest.y, Chest.endImage);
    Chest.gameElement.body.immovable = true;
    if(player.health < 100)
    {
        player.gold += Chest.gold/2;
        player.health = 100;
    }
    else
        player.gold += Chest.gold;
    this.goldText.setText('Gold: ' + player.gold);
    this.healthText.setText(this.player.name + "s Health: " + player.health);
}

/**
 * Function will open door that it is connected too, pressed is changed,
 * so that when pressure on the plates is false it will close the door.
 * @param PressurePlate - pressure plate object.
 * @param Door - door object to change.
 */
Levels.prototype.platePressed = function(PressurePlate,Door)
{
    if(PressurePlate.pressed == false)
    {
        Door.gameElement.kill();
        this.openDoor(Door);
    }
    PressurePlate.pressure = true;
    PressurePlate.pressed = true;
}

/**
 * Function will close the door the plate is connected to. Will
 * change the image of the door.
 * @param PressurePlate - pressure plate object.
 * @param Door - door to change.
 */
Levels.prototype.plateUnPressed = function(PressurePlate, Door)
{
    Door.gameElement.kill();
    Door.gameElement = this.level.create(Door.x, Door.y, Door.image);
    Door.gameElement.body.immovable = true;
    PressurePlate.pressed = false;
}

/**
 * Function will open the door and change its image.
 * @param Door - door object.
 */
Levels.prototype.openDoor = function(Door)
{
    Door.gameElement.kill();
    Door.gameElement = this.game.add.sprite(Door.x, Door.y, Door.endImage);
}

/**
 * Checks if an object inside another object.
 * @param GameObject - object 1 to check against object 2.
 * @param GameObject2 - object 2 to check against object 1
 * @returns {boolean} returns true if object 1 is inside object 2, false otherwise.
 */
Levels.prototype.isInside = function(GameObject, GameObject2)
{
    if((GameObject.x > GameObject2.x && GameObject.x + GameObject.length < GameObject2.x + GameObject2.length) &&
        (GameObject.y > GameObject2.y && GameObject.y + GameObject.height < GameObject2.y + GameObject2.height))
        return true;
    return false;
}