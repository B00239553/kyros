/**
 *   Author: B00239553
 */

/**
 * Used to create door.
 * @param x - position of door.
 * @param y - position of door.
 * @param image - image for the door when closed.
 * @param gameElement - phaser element for chest, used for collisions.
 * @param length - length of door along x axis.
 * @param height - height of door along x axis.
 * @param endImage - image for the door when open.
 * @constructor
 */
function Door(x, y, image, gameElement, length, height, endImage)
{
    GameObject.apply(this, arguments);
    this.endImage = endImage;
}

/**
 * Inheritance from GameObject.
 * @type {GameObject}
 */
Door.prototype = new GameObject();