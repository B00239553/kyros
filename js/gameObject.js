/**
 *   Author: B00239553
 */

/**
 * Used to create game objects, other classes can also inherit from this class.
 * @param x - position of object.
 * @param y - position of object.
 * @param image - image for the object.
 * @param gameElement - phaser element for object, used for collisions.
 * @param length - length of object along x axis.
 * @param height - height of object along x axis.
 * @constructor
 */
function GameObject(x, y, image, gameElement, length, height)
{
    this.x = x;
    this.y = y;
    this.image = image;
    this.gameElement = gameElement;
    this.length = length;
    this.height = height;
}