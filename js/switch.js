/**
 *   Author: B00239553
 */

/**
 * Used to create switch.
 * @param x - position of switch.
 * @param y - position of switch.
 * @param image - image for the switch.
 * @param gameElement - phaser element for switch, used for collisions.
 * @param length - length of switch along x axis.
 * @param height - height of switch along x axis.
 * @param endImage - image for the switch when on.
 * @param on - boolean to know if switch is on or off.
 * @constructor
 */
function Switch(x, y, image, gameElement, length, height, endImage, on)
{
    GameObject.apply(this, arguments);
    this.endImage = endImage;
    this.on = on;
}

/**
 * Inheritance from GameObject.
 * @type {GameObject}
 */
Switch.prototype = new GameObject();