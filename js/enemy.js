/**
 *   Author: B00239553
 */

/**
 * Enemy class, used for creating enemies.
 * @param x - position of enemy.
 * @param y - position of enemy.
 * @param image - image of enemy.
 * @param gameElement - phaser element of enemy. Used for collisions and animations.
 * @param length - length of enemy.
 * @param height - height of enemy.
 * @param health - health of enemy.
 * @param gold - gold enemy holds.
 * @param speed - how fast enemy can move.
 * @param reach - how far enemy can reach.
 * @param zoneDistance - distance enemy will start to find player.
 * @param hitChance - chance to hit player.
 * @param minDamage - minimum damage the enemy can inflict.
 * @param maxDamage - maximum damage the enemy can inflict.
 * @constructor
 */
function Enemy(x, y, image, gameElement, length, height, health, gold, speed, reach, zoneDistance, hitChance, minDamage, maxDamage)
{
    GameObject.apply(this, arguments);
    this.health = health;
    this.hasMovedX = 0;
    this.hasMovedY = 0;
    this.gold = gold;
    this.speed = speed;
    this.reach = reach;
    this.zoneDistance = zoneDistance;
    this.hitChance = hitChance;
    this.minDamage = minDamage;
    this.maxDamage = maxDamage;
    this.delayAttack = new Date().getTime();      //used to hold time for delaying enemy attack after striked.
    this.time = new Date().getTime();             //time.
    this.isBlocked = false;                       //boolean to know if path blocked.
    this.blockedTimeSet = false;                  //boolean to know if time set for path being blocked.
    this.blockedTime = null;                      //boolean to know how long enemy blocked for.
    this.pathFound = false;                       //boolean to know if a path has been found.
    this.direction1Number = null;                 //number to hold movement distance.
    this.direction1 = null;                       //string to hold movement direction.
    this.direction2Number = null;                 //number to hold movement distance.
    this.direction2 = null;                       //string to hold movement direction.
    this.count = Math.floor(Math.random() * 800); //random value to set enemy random movement.
}

/**
 * Inherits GameObject.
 * @type {GameObject}
 */
Enemy.prototype = new GameObject();

/**
 * Function to move AI up.
 */
Enemy.prototype.moveUp = function()
{
    this.gameElement.body.velocity.y -= this.speed;
    this.gameElement.animations.play('up');
    if(this.gameElement.body.y <= this.y)
        this.hasMovedY += this.y - this.gameElement.body.y;
    else
        this.hasMovedY += this.gameElement.body.y - this.y;
    this.lastDirection = "Up";
}

/**
 * Function to move AI down.
 */
Enemy.prototype.moveDown = function()
{
    this.gameElement.body.velocity.y += this.speed;
    this.gameElement.animations.play('down');
    if(this.gameElement.y <= this.y)
        this.hasMovedY += this.y - this.gameElement.y;
    else
        this.hasMovedY += this.gameElement.y - this.y;
    this.lastDirection = "Down";
}

/**
 * Function to move AI right.
 */
Enemy.prototype.moveRight = function()
{
    this.gameElement.body.velocity.x += this.speed;
    this.gameElement.animations.play('right');
    this.hasMovedX++;
    if(this.gameElement.x <= this.x)
        this.hasMovedX += this.x - this.gameElement.x;
    else
        this.hasMovedX += this.gameElement.x - this.x;
    this.lastDirection = "Right";
}

/**
 * Function to move AI left.
 */
Enemy.prototype.moveLeft = function()
{
    this.gameElement.body.velocity.x -= this.speed;
    this.gameElement.animations.play('left');
    this.hasMovedX++;
    if(this.gameElement.x <= this.x)
        this.hasMovedX += this.x - this.gameElement.x;
    else
        this.hasMovedX += this.gameElement.x - this.x;
    this.lastDirection = "Left";
}

/**
 * Function to attack player. Will calculate random hitChance and damage if
 * has chance to hit. Will change animation based on enemy and player position.
 * @param player - the players object.
 */
Enemy.prototype.attack = function(player)
{
    var hitChance = Math.floor(Math.random() * this.hitChance);
    if(hitChance > 0)
    {
        var damage = Math.floor((Math.random() * this.maxDamage) + this.minDamage);
        player.health -= damage;
    }
    if (this.x + this.length/2 <= player.x && this.y - this.height/2 >= player.y)
        this.gameElement.animations.play('attackUpRight');
    else if (this.x + this.length/2 >= player.x && this.y - this.height/2 >= player.y)
        this.gameElement.animations.play('attackUpLeft');
    else if (this.x + this.length/2 <= player.x && this.y - this.height/2 <= player.y)
        this.gameElement.animations.play('attackDownRight');
    else if (this.x + this.length/2 >= player.x && this.y - this.height/2 <= player.y)
        this.gameElement.animations.play('attackDownLeft');
}

/**
 * Checks if enemy is touching object taken in.
 * @param levelObject - the level object to check against.
 * @returns {boolean} returns true if touching, false otherwise.
 */
Enemy.prototype.isTouching = function(levelObject)
{
    if((this.x - this.reach <= levelObject.x + levelObject.length && this.x - this.reach >= levelObject.x) &&
        (this.y > levelObject.y && this.y < levelObject.y + levelObject.height ||
            this.y + this.height > levelObject.y && this.y + this.height < levelObject.y + levelObject.height))
        return true;
    if((this.x + this.length + this.reach >= levelObject.x && this.x + this.length + this.reach <= levelObject.x + levelObject.length) &&
        (this.y > levelObject.y && this.y < levelObject.y + levelObject.height ||
            this.y + this.height > levelObject.y && this.y + this.height < levelObject.y + levelObject.height))
        return true;
    if((this.y + this.height + this.reach >= levelObject.y && this.y + this.height + this.reach <= levelObject.y + levelObject.height) &&
        (this.x > levelObject.x && this.x < levelObject.x + levelObject.length ||
            this.x + this.length > levelObject.x && this.x + this.length < levelObject.x + levelObject.length))
        return true;
    if((this.y - this.reach <= levelObject.y + levelObject.height && this.y - this.reach >= levelObject.y) &&
        (this.x > levelObject.x && this.x < levelObject.x + levelObject.length ||
            this.x + this.length > levelObject.x && this.x + this.length < levelObject.x + levelObject.length))
        return true;
    return false;
}

/**
 * Checks if player is in enemy zone.
 * @param player - the player object.
 * @returns {boolean} returns true if player is zone, false otherwise.
 */
Enemy.prototype.inZone = function(player)
{
    if((player.x > this.x - this.zoneDistance && player.x + player.length < this.x + this.length + this.zoneDistance) &&
        (player.y > this.y - this.zoneDistance && player.y + this.height < this.y + this.height + this.zoneDistance))
        return true
    return false;
}

/**
 * Sets enemy attributes if player is not in their zone.
 */
Enemy.prototype.notInZone = function()
{
    this.gameElement.body.velocity.x = 0;
    this.gameElement.body.velocity.y = 0;
    this.pathFound = false;
    this.count++;
    if(this.count < 50)
        this.moveRight();
    else if(this.count < 100)
        this.moveUp();
    else if(this.count < 150)
        this.moveLeft();
    else if(this.count < 200)
        this.moveDown();
    else if(this.count < 800)
    {
        this.gameElement.frame = 19;

        this.x = this.gameElement.body.x;
        this.y = this.gameElement.body.y;
    }
    else
        this.count = 0;
}

//Used to change AI behaviour if player is very close to enemy, used
//to move enemy in distance to touch player so can attack.
/**
 * Used to change AI behaviour if player is very close to enemy.
 * @param player - the player object.
 * @returns {boolean} returns true is close by, false otherwise.
 */
Enemy.prototype.closeBy = function(player)
{
    if(((this.x + this.length - player.x)*(this.x + this.length - player.x)) +
        ((this.y + this.height - player.y)*(this.y + this.height - player.y)) < 32*32)
        return true;
    if(((this.x + this.length - player.x)*(this.x + this.length - player.x)) +
        ((this.y - (player.y + player.height))*(this.y - (player.y + player.height))) < 32*32)
        return true;
    if(((this.x - (player.x + player.length))*(this.x - (player.x + player.length))) +
        ((this.y + this.height - player.y)*(this.y + this.height - player.y)) < 32*32)
        return true;
    if(((this.x - (player.x + player.length))*(this.x - (player.x + player.length))) +
        ((this.y - (player.y + player.height))*(this.y - (player.y + player.height))) < 32*32)
        return true;
    return false;
}

/**
 * Sets enemy action for movements and attack.
 * @param game - the game object.
 */
Enemy.prototype.enemyActions = function(game)
{
    game.physics.arcade.enable(this.gameElement);
    this.gameElement.animations.add('left', [24, 25, 26, 27, 28, 29, 30, 31], 10, true);
    this.gameElement.animations.add('right', [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
    this.gameElement.animations.add('down', [16, 17, 18, 19, 20, 21, 22, 23], 10, true);
    this.gameElement.animations.add('up', [8, 9, 10, 11, 12, 13, 14, 15], 10, true);
    this.gameElement.animations.add('attackUpRight', [32, 33, 34], 10, true);
    this.gameElement.animations.add('attackUpLeft', [35, 36, 37], 10, true);
    this.gameElement.animations.add('attackDownRight', [38, 39, 40], 10, true);
    this.gameElement.animations.add('attackDownLeft', [41, 42, 43], 10, true);
}

/**
 * Finds the player, AI varys depending on circumstances.
 * @param player - the player object.
 * @param levelObjects - the level objects that an enemy can collide with.
 */
Enemy.prototype.findPlayer = function(player, levelObjects)
{
    this.time = new Date().getTime(); //used to know when one second has passed.
    this.gameElement.body.velocity.x = 0;  this.gameElement.body.velocity.y = 0;
    if(this.gameElement.body.x == this.x && this.gameElement.body.y == this.y && this.blockedTimeSet == false)
    {//Sets a timer, so know if enemy has being blocked for a tenth of a second.
        this.blockedTime = new Date().getTime() + 100;
        this.blockedTimeSet = true;
    }
    //Sets enemy is blocked as timer passed tenth of a second and is still blocked, only if enemy is not attacking.
    if(this.gameElement.body.x == this.x && this.gameElement.body.y == this.y && this.time > this.blockedTime && this.lastDirection != "Attacking")
        this.isBlocked = true;
    //Resets boolean to know that blocked time has been set.
    if(this.time > this.blockedTime)
        this.blockedTimeSet = false;
    //If enemy has been blocked and a path is found will update direction numbers that hold how far the enemy has to move to unblock itself.
    if(this.pathFound == true)
    {
        if(this.direction1Number > 0)
        {
            if(this.direction1 == "Up")
                this.direction1Number -= (this.y - this.gameElement.body.y);
            else if(this.direction1 == "Down")
                this.direction1Number -= (this.gameElement.body.y + this.height - (this.y + this.height));
            else if(this.direction1 == "Left")
                this.direction1Number -= (this.x - this.gameElement.body.x);
            else
                this.direction1Number -= (this.gameElement.body.x + this.length - (this.x + this.length));
        }
        else
        {
            if(this.direction2 == "Up")
                this.direction2Number -= (this.y - this.gameElement.body.y);
            else if(this.direction2 == "Down")
                this.direction2Number -= (this.gameElement.body.y + this.height - (this.y + this.height));
            else if(this.direction2 == "Left")
                this.direction2Number -= (this.x - this.gameElement.body.x);
            else
                this.direction2Number -= (this.gameElement.body.x + this.length - (this.x + this.length));
        }
    }

    this.x = this.gameElement.body.x;      this.y = this.gameElement.body.y;
    if(this.isTouching(player)) //if touching, attack
    {
        this.lastDirection = "Attacking";
        if(this.time > this.delayAttack) //checks if one second has passed since last attack.
        {
            this.attack(player);
            this.delayAttack = new Date().getTime() + 1000;
        }
        this.pathFound = false;
    }
    else if(this.isBlocked == true)//will find object enemy is blocked against then calls find path.
    {
        var i = 1, found = false;//i is set as one as map is always set as first object.
        while(i < levelObjects.length && found == false)
        {
            if(this.isTouching(levelObjects[i]) == true)
                found = true;
            else
                i++;
        }
        this.findPath(levelObjects[i], player);
        this.isBlocked = false;
    }
    else if(this.pathFound == true)//as path has been found calls function to use it.
    {
        this.usePath();
    }
    else if(this.closeBy(player) == true)//moves enemy differently as closer by.
    {
        if(this.x - this.length/2 <= player.x && this.x + this.length + this.length/2 > player.x + player.length)
            if(this.y > player.y + player.height) //only moves up or down if aligned in x axis.
                this.moveUp();
            else
                this.moveDown();
        else
            if(this.x + this.length < player.x + player.length)
                this.moveRight();
            else
                this.moveLeft();
    }
    else if((this.x - player.x)*(this.x - player.x) < (this.y - player.y)*(this.y - player.y) && this.hasMovedX == 0)
    {
        //checks the distance between objects and moves in which direction which is furtherest away.
        //Uses hasMovedX so player moves to grid like player, up, down, left or right.
        if(this.y > player.y + player.height)
            this.moveUp();
        else
            this.moveDown();
        if(this.hasMovedY > 64)
            this.hasMovedY = 0;
    }
    else
    {
        if(this.x + this.length < player.x + player.length)
            this.moveRight();
        else
            this.moveLeft();
        if(this.hasMovedX > 64)
            this.hasMovedX = 0;
    }
}

/**
 * Kills enemy using phaser function and then deletes this enemy.
 */
Enemy.prototype.kill = function()
{
    this.gameElement.kill();
    delete this;
}

/**
 * Will check which side of enemy is blocked, will then work out the distance from each corner to player
 * and set variables so knows which directions to take and how long for so enemy can unblock itself to
 * get to the player.
 * @param levelObject - the level object that is blocking the enemies path.
 * @param player - the player object.
 */
Enemy.prototype.findPath = function(levelObject, player)
{
    this.pathFound = true;
    var whereBlocked = null;

    //Find which direction is blocked.
    var direction;
    if((this.x - this.reach <= levelObject.x + levelObject.length && this.x - this.reach >= levelObject.x) &&
        (this.y > levelObject.y && this.y < levelObject.y + levelObject.height ||
            this.y + this.height > levelObject.y && this.y + this.height < levelObject.y + levelObject.height))
        whereBlocked = "Left";
    if((this.x + this.length + this.reach >= levelObject.x && this.x + this.length + this.reach <= levelObject.x + levelObject.length) &&
        (this.y > levelObject.y && this.y < levelObject.y + levelObject.height ||
            this.y + this.height > levelObject.y && this.y + this.height < levelObject.y + levelObject.height))
        whereBlocked = "Right";
    if((this.y + this.height + this.reach >= levelObject.y && this.y + this.height + this.reach <= levelObject.y + levelObject.height) &&
        (this.x > levelObject.x && this.x < levelObject.x + levelObject.length ||
            this.x + this.length > levelObject.x && this.x + this.length < levelObject.x + levelObject.length))
        whereBlocked = "Down";
    if((this.y - this.reach <= levelObject.y + levelObject.height && this.y - this.reach >= levelObject.y) &&
        (this.x > levelObject.x && this.x < levelObject.x + levelObject.length ||
            this.x + this.length > levelObject.x && this.x + this.length < levelObject.x + levelObject.length))
        whereBlocked = "Up";

    var playerCentreX, playerCentreY, corner1dist, corner2dist;
    playerCentreX = player.x + player.length/2;
    playerCentreY = player.y + player.height/2;
    //Depending where object is blocked, work out distance from the enemy to object corners and then to player.
    //Set distance needs to travel and direction so can use if another function to make the AI move.
    if(whereBlocked == "Left")
    {
        this.direction2 = "Left";
        corner1dist = (levelObject.x + levelObject.length - playerCentreX)*(levelObject.x + levelObject.length - playerCentreX) +
            (levelObject.y - playerCentreY)*(levelObject.y - playerCentreY);
        corner2dist = (levelObject.x + levelObject.length - playerCentreX)*(levelObject.x + levelObject.length - playerCentreX) +
            (levelObject.y + levelObject.height - playerCentreY)*(levelObject.y + levelObject.height - playerCentreY);
        if(corner1dist <= corner2dist)
        {
            this.direction1Number = (this.y - levelObject.y) + this.height;
            this.direction1 = "Up";
        }
        else
        {
            this.direction1Number = (levelObject.y + levelObject.height - (this.y + this.height)) + this.height;
            this.direction1 = "Down";
        }
        this.direction2Number = 32;
    }
    else if(whereBlocked == "Right")
    {
        this.direction2 = "Right";
        corner1dist = (levelObject.x - playerCentreX)*(levelObject.x - playerCentreX) +
            (levelObject.y - playerCentreY)*(levelObject.y - playerCentreY);
        corner2dist = (levelObject.x - playerCentreX)*(levelObject.x - playerCentreX) +
            (levelObject.y + levelObject.height - playerCentreY)*(levelObject.y + levelObject.height - playerCentreY);
        if(corner1dist <= corner2dist)
        {
            this.direction1Number = (this.y - levelObject.y) + this.height;
            this.direction1 = "Up";
        }
        else
        {
            this.direction1Number = (levelObject.y + levelObject.height - (this.y + this.height)) + this.height;
            this.direction1 = "Down";
        }
        this.direction2Number = 32;
    }
    else if(whereBlocked == "Up")
    {
        this.direction2 = "Up";
        corner1dist = (levelObject.x - playerCentreX)*(levelObject.x - playerCentreX) +
            (levelObject.y + levelObject.height - playerCentreY)*(levelObject.y + levelObject.height - playerCentreY);
        corner2dist = (levelObject.x + levelObject.length - playerCentreX)*(levelObject.x + levelObject.length - playerCentreX) +
            (levelObject.y + levelObject.height - playerCentreY)*(levelObject.y + levelObject.height - playerCentreY);
        if(corner1dist <= corner2dist)
        {
            this.direction1Number = (this.x - levelObject.x) + this.length;
            this.direction1 = "Left";
        }
        else
        {
            this.direction1Number = (levelObject.x + levelObject.length - (this.x + this.length)) + this.length;
            this.direction1 = "Right";
        }
        this.direction2Number = 32;
    }
    else if(whereBlocked == "Down")
    {
        this.direction2 = "Down";
        corner1dist = (levelObject.x - playerCentreX)*(levelObject.x - playerCentreX) +
            (levelObject.y - playerCentreY)*(levelObject.y - playerCentreY);
        corner2dist = (levelObject.x + levelObject.length - playerCentreX)*(levelObject.x + levelObject.length - playerCentreX) +
            (levelObject.y - playerCentreY)*(levelObject.y - playerCentreY);
        this.x = this.x;
        if(corner1dist <= corner2dist)
        {
            this.direction1Number = (this.x - levelObject.x) + this.length;
            this.direction1 = "Left";
        }
        else
        {
            this.direction1Number = (levelObject.x + levelObject.length - (this.x + this.length)) + this.length;
            this.direction1 = "Right";
        }
        this.direction2Number = 32;
    }
}

/**
 * From variables set by findPath(), this function will tell the enemy to move in
 * the first direction until the direction has been covered, this will then set the enemy
 * to move in the second direction, when both directions have been completed it resets
 * and will not be called upon again unless enemy gets blocked again.
 */
Enemy.prototype.usePath = function()
{
    if(this.direction1Number <= 0 && this.direction2Number <=0)
    {
        this.pathFound = false;
        this.direction1 = null;
        this.direction1Number = 0;
        this.direction2 = null;
        this.direction2Number = 0;
    }
    else
    {
        if(this.direction1Number > 0)
        {
            if(this.direction1 == "Up")
                this.moveUp();
            else if(this.direction1 == "Down")
                this.moveDown();
            else if(this.direction1 == "Left")
                this.moveLeft();
            else
                this.moveRight();
        }
        else
        {
            if(this.direction2 == "Up")
                this.moveUp();
            else if(this.direction2 == "Down")
                this.moveDown();
            else if(this.direction2 == "Left")
                this.moveLeft();
            else
                this.moveRight();
        }
    }
}