/**
 *   Author: B00239553
 */

/**
 * Level4, sets variables taken in and creates an array to hold objects for this level.
 * Creates variables for enemies and time.
 * @param game - game object.
 * @param player - player object.
 * @param level - level object.
 * @constructor
 */
function Level4(game, player, level)
{
    Levels.apply(this, arguments);
    this.levelObjects = [
    map = new GameObject(0,0,'L4Map',null,800,640),                      leftWall = new GameObject(32,64,'L4L',null,32,512),
    rightWall = new GameObject(736,128,'L4R',null,32,512),               bottomWall = new GameObject(0,576,'L4B',null,672,64),
    topWall = new GameObject(64,32,'L4T',null,672,32),                   midWall1 = new GameObject(64,256,'L4M1',null,64,64),
    midWall2 = new GameObject(64,416,'L4M2',null,192,64),                midWall3 = new GameObject(192,256,'L4M3',null,64,64),
    midWall4 = new GameObject(256,160,'L4M4', null,64,320),              midWall5 = new GameObject(320,160,'L4M1',null,64,64),
    midWall6 = new GameObject(448,160,'L4M5',null,288,64),               midWall7 = new GameObject(512,224,'L4M6',null,64,256),
    midWall8 = new GameObject(576,288,'L4M1',null,64,64),                midWall9 = new GameObject(704,288,'L4M7',null,32,64),
    chest1 = new Chest(96,384,'chestCD',null,32,32,'chestOD',false, 80), chest2 = new Chest(192,384,'chestCD',null,32,32,'chestOD',false, 80),
    chest3 = new Chest(640,224,'chestC',null,32,32,'chestO',false, 80),  door = new Door(640,288,'door2C',null,0,0,'door2O'),
    entry = new GameObject(736,64,'gateCR',null,64,64),                  plates = new PressurePlate(576,352,'plates',null,64,64,false,false),
    block1 = new GameObject(128,256,'block',null,32,32),                 block2 = new GameObject(160,256,'block',null,32,32),
    block3 = new GameObject(384,160,'block',null,32,32),                 block4 = new GameObject(416,160,'block',null,32,32),
    block5 = new GameObject(384,192,'block',null,32,32),                 block6 = new GameObject(416,192,'block',null,32,32),
    block7 = new GameObject(576,448,'block',null,32,32),                 block8 = new GameObject(576,480,'block',null,32,32),
    block9 = new GameObject(576,512,'block',null,32,32),                 block10 = new GameObject(576,544,'block',null,32,32),
    block11 = new GameObject(608,448,'theBlock',null,32,32),             block12 = new GameObject(640,448,'block',null,32,32),
    block13 = new GameObject(672,448,'block',null,32,32),                block14 = new GameObject(704,448,'block',null,32,32),
    exit = new GameObject(672,576,'gateO',null,64,64)];
    var enemy1, enemy1Alive, enemy1Text, enemy2, enemy2Alive, enemy2Text, enemy3, enemy3Alive, enemy3Text, time, delayPlayerAttack;
}

/**
 * Inherits levels.
 * @type {Levels}
 */
Level4.prototype = new Levels();

/**
 * Creates all objects for the level.
 */
Level4.prototype.create = function()
{
    for(var i = 0; i < this.levelObjects.length; i++)
    {
        if(this.levelObjects[i].image == 'gateO')//Set so player will walk under gate.
        {
            this.player.gameElement = this.game.add.sprite(this.game.world.width - 96, 96, 'Kyros');
            this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
        }
        else if (this.levelObjects[i].image == 'theBlock')//Set so block will render on top of plates.
        {
            this.levelObjects[i].gameElement = this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
            this.game.physics.arcade.enable(this.levelObjects[i].gameElement);
        }
        else if(this.levelObjects[i].image == 'plates')//Set so plate will not move.
        {
            this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
        }
        else
        {
            this.levelObjects[i].gameElement = this.level.create(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
            if(this.levelObjects[i].image != 'block')//Set so will allow block to be moved.
                this.levelObjects[i].gameElement.body.immovable = true;
            else
                this.levelObjects[i].gameElement.body.immovable = false;
        }
    }
    this.player.playerActions(this.game);
    //Create enemies for this level, health to be displayed at top of screen, boolean held to know if alive.
    enemy1 = new Enemy(192, 192, 'Grunts', null, 32, 32, 100, 60, 56, 4, 250, 4, 10, 5);
    enemy1.gameElement = this.game.add.sprite(enemy1.x, enemy1.y, enemy1.image);
    enemy1.enemyActions(this.game);
    enemy1Alive = true;
    enemy2 = new Enemy(96, 512, 'Grunts', null, 32, 32, 100, 60, 56, 4, 250, 4, 10, 5);
    enemy2.gameElement = this.game.add.sprite(enemy2.x, enemy2.y, enemy2.image);
    enemy2.enemyActions(this.game);
    enemy2Alive = true;
    enemy3 = new Enemy(448, 288, 'Grunts', null, 32, 32, 100, 60, 56, 4, 250, 4, 10, 5);
    enemy3.gameElement = this.game.add.sprite(enemy3.x, enemy3.y, enemy3.image);
    enemy3.enemyActions(this.game);
    enemy3Alive = true;
    delayPlayerAttack = new Date().getTime();
    time = new Date().getTime();
    this.goldText = this.game.add.text(5, 22, 'Gold: ' + this.player.gold, { fontSize: '32px', fill: 'gold', stroke: "black", strokeThickness: 5 });
    this.goldText.scale.setTo(0.6, 0.6);
    this.healthText = this.game.add.text(5, 5, this.player.name + "'s Health: " + this.player.health, { fontSize: '32px', fill: 'red', stroke: "black", strokeThickness: 5 });
    this.healthText.scale.setTo(0.6, 0.6);
    enemy1Text = this.game.add.text(220, 5, "Enemy1 Health: " + enemy1.health, { fontSize: '32px', fill: 'blue', stroke: "black", strokeThickness: 5 });
    enemy1Text.scale.setTo(0.6, 0.6);
    enemy2Text = this.game.add.text(400, 5, "Enemy2 Health: " + enemy2.health, { fontSize: '32px', fill: 'blue', stroke: "black", strokeThickness: 5 });
    enemy2Text.scale.setTo(0.6, 0.6);
    enemy3Text = this.game.add.text(580, 5, "Enemy3 Health: " + enemy3.health, { fontSize: '32px', fill: 'blue', stroke: "black", strokeThickness: 5 });
    enemy3Text.scale.setTo(0.6, 0.6);
}

/**
 * Update function. Sets collisions between all objects and player.
 * Resets velocity of block so will not constantly move.
 * Calls appropriate functions when player interacts with objects.
 * Checks if player interacts with enemies and takes appropriate action.
 * @param actionKey - key to be used to create actions against objects.
 */
Level4.prototype.update = function(actionKey)
{
    this.game.physics.arcade.collide(this.player.gameElement, this.level);
    this.game.physics.arcade.collide(this.level, this.level);
    this.game.physics.arcade.collide(this.player.gameElement, block11.gameElement);
    this.game.physics.arcade.collide(block11.gameElement, this.level);
    if(enemy1Alive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, enemy1.gameElement);
        this.game.physics.arcade.collide(enemy1.gameElement, this.level);
        this.game.physics.arcade.collide(enemy1.gameElement, block11.gameElement);
    }
    if(enemy2Alive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, enemy2.gameElement);
        this.game.physics.arcade.collide(enemy2.gameElement, this.level);
        this.game.physics.arcade.collide(enemy2.gameElement, block11.gameElement);
    }
    if(enemy3Alive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, enemy3.gameElement);
        this.game.physics.arcade.collide(enemy3.gameElement, this.level);
        this.game.physics.arcade.collide(enemy3.gameElement, block11.gameElement);
    }
    block1.gameElement.body.velocity.x = 0;    block1.gameElement.body.velocity.y = 0;
    block2.gameElement.body.velocity.x = 0;    block2.gameElement.body.velocity.y = 0;
    block3.gameElement.body.velocity.x = 0;    block3.gameElement.body.velocity.y = 0;
    block4.gameElement.body.velocity.x = 0;    block4.gameElement.body.velocity.y = 0;
    block5.gameElement.body.velocity.x = 0;    block5.gameElement.body.velocity.y = 0;
    block6.gameElement.body.velocity.x = 0;    block6.gameElement.body.velocity.y = 0;
    block7.gameElement.body.velocity.x = 0;    block7.gameElement.body.velocity.y = 0;
    block8.gameElement.body.velocity.x = 0;    block8.gameElement.body.velocity.y = 0;
    block9.gameElement.body.velocity.x = 0;    block9.gameElement.body.velocity.y = 0;
    block10.gameElement.body.velocity.x = 0;   block10.gameElement.body.velocity.y = 0;
    block11.gameElement.body.velocity.x = 0;   block11.gameElement.body.velocity.y = 0;
    block12.gameElement.body.velocity.x = 0;   block12.gameElement.body.velocity.y = 0;
    block13.gameElement.body.velocity.x = 0;   block13.gameElement.body.velocity.y = 0;
    block14.gameElement.body.velocity.x = 0;   block14.gameElement.body.velocity.y = 0;
    block11.x = block11.gameElement.body.x;    block11.y = block11.gameElement.body.y;
    this.player.moveBlock(this.game, block11);
    this.player.moveBlock(this.game, block2);
    this.player.moveBlock(this.game, block3);
    this.player.moveBlock(this.game, block4);
    this.player.moveBlock(this.game, block5);
    this.player.moveBlock(this.game, block6);
    this.player.moveBlock(this.game, block7);
    this.player.moveBlock(this.game, block8);
    this.player.moveBlock(this.game, block9);
    this.player.moveBlock(this.game, block10);
    this.player.moveBlock(this.game, block11);
    this.player.moveBlock(this.game, block12);
    this.player.moveBlock(this.game, block13);
    this.player.moveBlock(this.game, block14);
    if(actionKey.isDown && this.player.isTouching(chest1) == true && chest1.open == false)
        this.openChest(chest1, this.player);
    if(actionKey.isDown && this.player.isTouching(chest2) == true && chest2.open == false)
        this.openChest(chest2, this.player);
    if(actionKey.isDown && this.player.isTouching(chest3) == true && chest3.open == false)
        this.openChest(chest3, this.player);
    if(this.isInside(block11, plates) == true)
        this.platePressed(plates, door);
    else if(this.isInside(this.player, plates) == true)
        this.platePressed(plates, door);
    else
        plates.pressure = false;
    if(plates.pressure == false && plates.pressed == true)
        this.plateUnPressed(plates, door);
    if(this.isInside(this.player, exit) == true)
    {
        this.completed = true;
        this.endLevel(false);
    }
    this.player.updatePlayer(this.game);
    //Deal with enemy movements and attack, also player attack.
    if(enemy1Alive == true)
    {
        if(enemy1.inZone(this.player) == true)
            enemy1.findPlayer(this.player, this.levelObjects);
        else
            enemy1.notInZone();
        if(this.player.isTouching(enemy1) == true && time > delayPlayerAttack)
        {
            this.player.attack(enemy1, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        enemy1Text.setText('Enemy1 Health: ' + enemy1.health);
        if(enemy1.health <= 0) //enemy killed.
        {
            this.player.gold += enemy1.gold;
            enemy1.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            enemy1Alive = false;
            enemy1Text.setText('');
            this.player.isAttacking = false;
        }
    }
    if(enemy2Alive == true)
    {
        if(enemy2.inZone(this.player) == true)
            enemy2.findPlayer(this.player, this.levelObjects);
        else
            enemy2.notInZone();
        if(this.player.isTouching(enemy2) == true && time > delayPlayerAttack)
        {
            this.player.attack(enemy2, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        enemy2Text.setText('Enemy2 Health: ' + enemy2.health);
        if(enemy2.health <= 0)
        {
            this.player.gold += enemy2.gold;
            enemy2.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            enemy2Alive = false;
            enemy2Text.setText('');
            this.player.isAttacking = false;
        }
    }
    if(enemy3Alive == true)
    {
        if(enemy3.inZone(this.player) == true)
            enemy3.findPlayer(this.player, this.levelObjects);
        else
            enemy3.notInZone();
        if(this.player.isTouching(enemy3) == true && time > delayPlayerAttack)
        {
            this.player.attack(enemy3, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        enemy3Text.setText('Enemy3 Health: ' + enemy3.health);
        if(enemy3.health <= 0)
        {
            this.player.gold += enemy3.gold;
            enemy3.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            enemy3Alive = false;
            enemy3Text.setText('');
            this.player.isAttacking = false;
        }
    }
    if(this.player.health <= 0)
    {
        this.player.health = 0;
        var uri = "http://mcm-highscores-hrd.appspot.com/";
        var url = uri + "score?game={0}&nickname={1}&email={2}&score={3}&func=?";
        url = url.replace('{0}', "Kyros");
        url = url.replace('{1}', this.player.name);
        url = url.replace('{2}', "oneilmike85@gmail.com");
        url = url.replace('{3}', this.player.gold);
        document.getElementById("url").innerText = url;
        $.ajax({
            type:  "GET",
            url:   url,
            async: true,
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function (json)
            {
                $("#result").text(json.result);
            },
            error: function (e) {
                window.alert(e.message);
            }
        });
        this.endLevel(true);
    }
    this.healthText.setText(this.player.name + "'s Health: " + this.player.health);
    time = new Date().getTime();
}

/**
 * Deletes level if player dies or completes level. Will delete
 * player only if is dead.
 * @param playerDead - boolean to know if player is dead or alive.
 */
Level4.prototype.endLevel = function(playerDead)
{
    if(playerDead == true)
        this.player.kill();
    this.level.removeAll();
    if(enemy1Alive == true)
    {
        enemy1.kill();
        enemy1Alive = false;
    }
    if(enemy2Alive == true)
    {
        enemy2.kill();
        enemy2Alive = false;
    }
    if(enemy3Alive == true)
    {
        enemy3.kill();
        enemy3Alive = false;
    }
    delete this;
}
