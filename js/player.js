/**
 *   Author: B00239553
 */

/**
 * Player class, used for holding the player attributes throughout the game.
 * @param x - position of player.
 * @param y - position of player.
 * @param image - image of player.
 * @param gameElement - phaser element of player. Used for collisions and animations.
 * @param length - length of player.
 * @param height - height of player.
 * @param health - health of player.
 * @param gold - gold player holds.
 * @param speed - how fast player can move.
 * @param reach - how far player can reach.
 * @constructor
 */
function Player(x, y, image, gameElement, length, height, health, gold, speed, reach)
{
    GameObject.apply(this, arguments);
    this.health = health;
    this.gold = gold;
    this.speed = speed;
    this.reach = reach;
    this.damage = null;
    this.isAttacking = false; //used to know if player is attacking.
    this.name = "Player"; //default set as player, player can input their name using textbox and button.
}

/**
 * Inherits GameObject.
 * @type {GameObject}
 */
Player.prototype = new GameObject();

/**
 * Sets player actions for movements and attack.
 * @param game - game object.
 */
Player.prototype.playerActions = function(game)
{
    game.physics.arcade.enable(this.gameElement);
    this.gameElement.animations.add('left', [24, 25, 26, 27, 28, 29, 30, 31], 10, true);
    this.gameElement.animations.add('right', [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
    this.gameElement.animations.add('down', [16, 17, 18, 19, 20, 21, 22, 23], 10, true);
    this.gameElement.animations.add('up', [8, 9, 10, 11, 12, 13, 14, 15], 10, true);
    this.gameElement.animations.add('attackUpRight', [32, 33, 34], 10, true);
    this.gameElement.animations.add('attackUpLeft', [35, 36, 37], 10, true);
    this.gameElement.animations.add('attackDownRight', [38, 39, 40], 10, true);
    this.gameElement.animations.add('attackDownLeft', [41, 42, 43], 10, true);
}

/**
 * Moves player based on cursor keys.
 * @param game - game object.
 */
Player.prototype.updatePlayer = function(game)
{
    //Reset player velocity.
    this.gameElement.body.velocity.x = 0;
    this.gameElement.body.velocity.y = 0;

    //Create movement controls.
    cursors = game.input.keyboard.createCursorKeys();
    if (cursors.left.isDown)//Move left.
    {
        this.gameElement.body.velocity.x -= this.speed;
        this.gameElement.animations.play('left');
        this.isAttacking = false;
    }
    else if (cursors.right.isDown)// Move right.
    {
        this.gameElement.body.velocity.x += this.speed;
        this.gameElement.animations.play('right');
        this.isAttacking = false;
    }
    else if (cursors.up.isDown)// Move up.
    {
        this.gameElement.body.velocity.y -= this.speed;
        this.gameElement.animations.play('up');
        this.isAttacking = false;
    }
    else if (cursors.down.isDown)//Move down.
    {
        this.gameElement.body.velocity.y += this.speed;
        this.gameElement.animations.play('down');
        this.isAttacking = false;
    }
    else if(this.isAttacking == false)//Stand still.
    {
        this.gameElement.animations.stop();
        this.gameElement.frame = 19;
    }
    //Used to update player GameObject per players movements.
    this.x = this.gameElement.body.x;
    this.y = this.gameElement.body.y;

}

/**
 * Attack function for player, will calculate chance to hit enemy,
 * if player can hit will calculate damage based on attributes.
 * Will also play animations for attack.
 * @param enemy - enemy to attack.
 * @param game - game object.
 */
Player.prototype.attack = function(enemy, game)
{
    var attackKey = game.input.keyboard.addKey(Phaser.Keyboard.X);
    if(attackKey.isDown)
    {
        var hitChance = Math.floor(Math.random() * 10);
        if(hitChance > 0)
        {
            this.damage = Math.floor((Math.random() * 10) + 15);
            enemy.health -= this.damage;
        }
        if (attackKey.isDown && this.x + this.length/2 <= enemy.x && this.y - this.height/2 >= enemy.y)
            this.gameElement.animations.play('attackUpRight');
        else if (attackKey.isDown && this.x + this.length/2 >= enemy.x && this.y - this.height/2 >= enemy.y)
            this.gameElement.animations.play('attackUpLeft');
        else if (attackKey.isDown && this.x + this.length/2 <= enemy.x && this.y - this.height/2 <= enemy.y)
            this.gameElement.animations.play('attackDownRight');
        else if (attackKey.isDown && this.x + this.length/2 >= enemy.x && this.y - this.height/2 <= enemy.y)
            this.gameElement.animations.play('attackDownLeft');
    }
    this.isAttacking = true; //used so animations work as player stopped when attacking.
}

/**
 * Checks if player is touching object taken in.
 * @param levelObject - level object to check if touching
 * @returns {boolean} returns true if touching object, false otherwise.
 */
Player.prototype.isTouching = function(levelObject)
{
    if((this.x - this.reach <= levelObject.x + levelObject.length && this.x - this.reach >= levelObject.x) &&
        (this.y > levelObject.y && this.y < levelObject.y + levelObject.height ||
            this.y + this.height > levelObject.y && this.y + this.height < levelObject.y + levelObject.height))
        return true;
    if((this.x + this.length + this.reach >= levelObject.x && this.x + this.length + this.reach <= levelObject.x + levelObject.length) &&
        (this.y > levelObject.y && this.y < levelObject.y + levelObject.height ||
            this.y + this.height > levelObject.y && this.y + this.height < levelObject.y + levelObject.height))
        return true;
    if((this.y + this.height + this.reach >= levelObject.y && this.y + this.height + this.reach <= levelObject.y + levelObject.height) &&
        (this.x > levelObject.x && this.x < levelObject.x + levelObject.length ||
            this.x + this.length > levelObject.x && this.x + this.length < levelObject.x + levelObject.length))
        return true;
    if((this.y - this.reach <= levelObject.y + levelObject.height && this.y - this.reach >= levelObject.y) &&
        (this.x > levelObject.x && this.x < levelObject.x + levelObject.length ||
            this.x + this.length > levelObject.x && this.x + this.length < levelObject.x + levelObject.length))
        return true;
    return false;
}

/**
 * Kills player using phaser function and then deletes object.
 */
Player.prototype.kill = function()
{
    this.gameElement.kill();
    delete this;
}

/**
 * Allows the player to move a blocks position, added as blocks may be
 * pushed against walls that they cannot be pushed away from.
 * @param game
 * @param block
 */
Player.prototype.moveBlock = function(game, block)
{
    if(this.isTouching(block) == true)
    {
        cursors = game.input.keyboard.createCursorKeys();
        var moveKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        if(cursors.down.isDown && moveKey.isDown)
        {
            block.gameElement.body.x = this.x;
            block.gameElement.body.y = this.y + this.height;
        }
        else if(cursors.up.isDown && moveKey.isDown)
        {
            block.gameElement.body.x = this.x;
            block.gameElement.body.y = this.y - this.height;
        }
        else if(cursors.right.isDown && moveKey.isDown)
        {
            block.gameElement.body.x = this.x + this.length;
            block.gameElement.body.y = this.y;
        }
        else if(cursors.left.isDown && moveKey.isDown)
        {
            block.gameElement.body.x = this.x - this.length;
            block.gameElement.body.y = this.y;
        }
    }
}