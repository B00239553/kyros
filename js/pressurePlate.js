/**
 *   Author: B00239553
 */

/**
 * Used to create pressure plate.
 * @param x - position of pressure plate.
 * @param y - position of pressure plate.
 * @param image - image for the pressure plate.
 * @param gameElement - phaser element for pressure plate, used for collisions.
 * @param length - length of pressure plate along x axis.
 * @param height - height of pressure plate along x axis.
 * @constructor
 */
function PressurePlate(x, y, image, gameElement, length, height, pressure, pressed)
{
    GameObject.apply(this, arguments);
    this.pressure = pressure;
    this.pressed = pressed;
}

/**
 * Inheritance from GameObject.
 * @type {GameObject}
 */
PressurePlate.prototype = new GameObject();