/**
 *   Author: B00239553
 */

/**
 * Level5, sets variables taken in and creates an array to hold objects for this level.
 * Creates variables for enemies and time.
 * @param game - game object.
 * @param player - player object.
 * @param level - level object.
 * @constructor
 */
function Level5(game, player, level)
{
    Levels.apply(this, arguments);
    this.levelObjects = [
    map = new GameObject(0,0,'L5Map',null,800,640),                            leftWall = new GameObject(0,32,'L5L',null,32,576),
    rightWall = new GameObject(768,0,'L5R',null,32,608),                       bottomWall = new GameObject(0,608,'L5B',null,800,32),
    topWall = new GameObject(0,0,'L5T',null,704,32),                           midWall1 = new GameObject(32,224,'L5M1',null,96,32),
    midWall2 = new GameObject(32,384,'L5M1',null,96,32),                       midWall3 = new GameObject(128,32,'L5M2',null,64,64),
    midWall4 = new GameObject(128,160,'L5M3',null,64,128),                     midWall5 = new GameObject(128,352,'L5M3',null,64,128),
    midWall6 = new GameObject(128,544,'L5M2',null,64,64),                      midWall7 = new GameObject(320,32,'L5M4',null,64,256),
    midWall8 = new GameObject(320,352,'L5M4',null,64,256),                     mdleWall9 = new GameObject(480,32,'L5M5',null,64,352),
    midWall10 = new GameObject(480, 448,'L5M6',null,64,160),                   midWall11 = new GameObject(640,32,'L5M7',null,64,448),
    midWall12 = new GameObject(640, 546,'L5M2',null,64,64),                    switch1 = new Switch(32,32,'switchOffR',null,32,32,'switchOnR',false),
    switch2 = new Switch(32,192,'switchOffR',null,32,32,'switchOnR',false),    switch3 = new Switch(192,256,'switchOffR',null,32,32,'switchOnR',false),
    switch4 = new Switch(192,352,'switchOffR',null,32,32,'switchOnR',false),   switch5 = new Switch(416,32,'switchOffD',null,32,32,'switchOnD',false),
    switch6 = new Switch(416,576,'switchOff',null,32,32,'switchOn',false),     switch7 = new Switch(576,32,'switchOffD',null,32,32,'switchOnD',false),
    chest1 = new Chest(192,32,'chestC',null,32,32,'chestO',false, 100),        chest2 = new Chest(288,32,'chestC',null,32,32,'chestO',false, 100),
    chest3 = new Chest(192,576,'chestCD',null,32,32,'chestOD',false, 100),     chest4 = new Chest(288,576,'chestCD',null,32,32,'chestOD',false, 100),
    door1 = new Door(128,96,'doorCL',null,0,0,'doorOL'),                       door2 = new Door(128,288,'doorCL',null,0,0,'doorOL'),
    door3 = new Door(128,480,'doorCL',null,0,0,'doorOL'),                      door4 = new Door(320,288,'doorCL',null,0,0,'doorOL'),
    door5 = new Door(480,384,'doorCL',null,0,0,'doorOL'),                      door6 = new Door(640,480,'doorCL',null,0,0,'doorOL'),
    entry = new GameObject(704,0,'gateC',null,64,64),                            plates1 = new PressurePlate(32,96,'plates',null,64,64,false,false),
    plates2 = new PressurePlate(546,546,'largePlates',null,96,64,false,false), plates3 = new PressurePlate(704,480,'plates',null,64,64,false,false),
    trident = new GameObject(64,304,'theTrident',null,32,32),                  block1 = new GameObject(64,448,'block',null,32,32),
    block2 = new GameObject(576,128,'block',null,32,32)];
    var boss, bossAlive, bossText, time, delayPlayerAttack;
}

/**
 * Inherits levels.
 * @type {Levels}
 */
Level5.prototype = new Levels();

/**
 * Creates all objects for the level.
 */
Level5.prototype.create = function()
{
    for(var i = 0; i < this.levelObjects.length; i++)
    {
        if(this.levelObjects[i].image == 'plates' || this.levelObjects[i].image == 'largePlates')//Set so plates will not move.
        {
            this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
        }
        else if (this.levelObjects[i].image == 'block')//Set so block will render on top of plates.
        {
            this.levelObjects[i].gameElement = this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
            this.game.physics.arcade.enable(this.levelObjects[i].gameElement);
        }
        else
        {
            this.levelObjects[i].gameElement = this.level.create(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
            this.levelObjects[i].gameElement.body.immovable = true;
        }
    }
    //Sets player, and their collisions and animations.
    this.player.gameElement = this.game.add.sprite(this.game.world.width - 64, 96, 'Kyros');
    this.player.playerActions(this.game);
    boss = new Enemy(224, 320, 'Boss', null, 32, 32, 200, 200, 16, 4, 64, 8, 15, 15);
    boss.gameElement = this.game.add.sprite(boss.x, boss.y, boss.image);
    boss.enemyActions(this.game);
    bossAlive = true;
    delayPlayerAttack = new Date().getTime();
    time = new Date().getTime();
    this.goldText = this.game.add.text(5, 22, 'Gold: ' + this.player.gold, { fontSize: '32px', fill: 'gold', stroke: "black", strokeThickness: 5 });
    this.goldText.scale.setTo(0.6, 0.6);
    this.healthText = this.game.add.text(5, 5, this.player.name + "'s Health: " + this.player.health, { fontSize: '32px', fill: 'red', stroke: "black", strokeThickness: 5 });
    this.healthText.scale.setTo(0.6, 0.6);
    bossText = this.game.add.text(220, 5, "Boss Health: " + boss.health, { fontSize: '32px', fill: 'green', stroke: "black", strokeThickness: 5 });
    bossText.scale.setTo(0.6, 0.6);
}

/**
 * Update function. Sets collisions between all objects and player.
 * Resets velocity of block so will not constantly move.
 * Calls appropriate functions when player interacts with objects.
 * Checks if player interacts with enemies and takes appropriate action.
 * @param actionKey - key to be used to create actions against objects.
 */
Level5.prototype.update = function(actionKey)
{
    this.game.physics.arcade.collide(this.player.gameElement, this.level);
    this.game.physics.arcade.collide(this.level, this.level);
    this.game.physics.arcade.collide(this.player.gameElement, block1.gameElement);
    this.game.physics.arcade.collide(block1.gameElement, this.level);
    this.game.physics.arcade.collide(this.player.gameElement, block2.gameElement);
    this.game.physics.arcade.collide(block2.gameElement, this.level);
    if(bossAlive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, boss.gameElement);
        this.game.physics.arcade.collide(boss.gameElement, this.level);
        this.game.physics.arcade.collide(boss.gameElement, block1.gameElement);
        this.game.physics.arcade.collide(boss.gameElement, block2.gameElement);
    }
    block1.gameElement.body.velocity.x = 0;    block1.gameElement.body.velocity.y = 0;
    block2.gameElement.body.velocity.x = 0;    block2.gameElement.body.velocity.y = 0;
    block1.x = block1.gameElement.body.x;      block1.y = block1.gameElement.body.y;
    block2.x = block2.gameElement.body.x;      block2.y = block2.gameElement.body.y;
    this.player.moveBlock(this.game, block1);
    this.player.moveBlock(this.game, block2);
    if(actionKey.isDown && this.player.isTouching(chest1) == true && chest1.open == false)
        this.openChest(chest1, this.player);
    if(actionKey.isDown && this.player.isTouching(chest2) == true && chest2.open == false)
        this.openChest(chest2, this.player);
    if(actionKey.isDown && this.player.isTouching(chest3) == true && chest3.open == false)
        this.openChest(chest3, this.player);
    if(actionKey.isDown && this.player.isTouching(chest4) == true && chest4.open == false)
        this.openChest(chest4, this.player);
    if(actionKey.isDown && this.player.isTouching(switch1) == true && switch1.on == false)
        this.flickSwitch(switch1);
    if(actionKey.isDown && this.player.isTouching(switch2) == true && switch2.on == false)
        this.flickSwitch(switch2);
    if(this.isInside(block1, plates1) && switch1.on == true && switch2.on == true)
        this.platePressed(plates1, door2);
    else if(this.isInside(this.player, plates1) && switch1.on == true && switch2.on == true)
        this.platePressed(plates1, door2);
    else
        plates1.pressure = false;
    if(plates1.pressure == false && plates1.pressed == true)
        this.plateUnPressed(plates1, door2);
    if(actionKey.isDown && this.player.isTouching(switch3) == true && switch3.on == false)
        this.flickSwitchDoor(switch3, door1);
    if(actionKey.isDown && this.player.isTouching(switch4) == true && switch4.on == false)
        this.flickSwitchDoor(switch4, door3);
    if(actionKey.isDown && this.player.isTouching(switch5) == true && switch5.on == false)
        this.flickSwitch(switch5);
    if(actionKey.isDown && this.player.isTouching(switch6) == true && switch6.on == false)
        this.flickSwitch(switch6);
    if(switch5.on == true && switch6.on == true)
        this.openDoor(door4);
    if(actionKey.isDown && this.player.isTouching(switch7) == true && switch7.on == false)
        this.flickSwitch(switch7);
    if(this.isInside(block2, plates2) == true && switch7.on == true)
        this.platePressed(plates2, door5);
    else if(this.isInside(this.player, plates2) == true && switch7.on == true)
        this.platePressed(plates2, door5);
    else
        plates2.pressure = false
    if(plates2.pressure == false && plates2.pressed == true)
        this.plateUnPressed(plates2, door5);
    if(this.isInside(this.player, plates3) == true)
        this.openDoor(door6);
    if(this.player.isTouching(trident) == true)
    {
        this.completed = true;
        this.endLevel();
    }
    this.player.updatePlayer(this.game);
    if(bossAlive == true)
    {
        if(boss.inZone(this.player) == true)
            boss.findPlayer(this.player, this.levelObjects);
        else
            boss.notInZone();
        if(this.player.isTouching(boss) == true && time > delayPlayerAttack)
        {
            this.player.attack(boss, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        bossText.setText('Boss Health: ' + boss.health);
        if(boss.health <= 0) //enemy killed.
        {
            this.player.gold += boss.gold;
            boss.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            bossAlive = false;
            bossText.setText('');
            this.player.isAttacking = false;
        }
    }
    if(this.player.health <= 0)
    {
        this.player.health = 0;
        this.endLevel();
    }
    this.healthText.setText(this.player.name + "'s Health: " + this.player.health);
    time = new Date().getTime();
}

/**
 * Deletes level if player dies or completes level. Will delete
 * player only if is dead.
 * @param playerDead - boolean to know if player is dead or alive.
 */
Level5.prototype.endLevel = function()
{
    var uri = "http://mcm-highscores-hrd.appspot.com/";
    var url = uri + "score?game={0}&nickname={1}&email={2}&score={3}&func=?";
    url = url.replace('{0}', "Kyros");
    url = url.replace('{1}', this.player.name);
    url = url.replace('{2}', "oneilmike85@gmail.com");
    url = url.replace('{3}', this.player.gold);
    document.getElementById("url").innerText = url;
    $.ajax({
        type:  "GET",
        url:   url,
        async: true,
        contentType: 'application/json',
        dataType: 'jsonp',
        success: function (json)
        {
            $("#result").text(json.result);
        },
        error: function (e) {
            window.alert(e.message);
        }
    });
    this.player.kill();
    this.level.removeAll();
    if(bossAlive == true)
    {
        boss.kill();
        bossAlive = false;
    }
    delete this;
}