/**
 *   Author: B00239553
 */

/**
 * Level3, sets variables taken in and creates an array to hold objects for this level.
 * Creates variables for enemies and time.
 * @param game - game object.
 * @param player - player object.
 * @param level - level object.
 * @constructor
 */
function Level3(game, player, level)
{
    Levels.apply(this, arguments);
    this.levelObjects = [
    map = new GameObject(0,0,'L3Map',null,800,640),                          leftWall = new GameObject(32,64,'L3L',null,32,448),
    rightWall = new GameObject(736,128,'L3R',null,32,448),                   bottomWall = new GameObject(64,576,'L3B',null,672,32),
    topWall = new GameObject(64,32,'L3T',null,672,32),                       midWall1 = new GameObject(64,352,'L3M1',null,64,64),
    midWall2 = new GameObject(192,352,'L3M2',null,64,224),                   midWall3 = new GameObject(416,512,'L3M3',null,64,64),
    midWall4 = new GameObject(416,64,'L3M4',null,64,384),                    chest1 = new Chest(352,64,'chestC',null,32,32,'chestO',false, 70),
    chest2 = new Chest(704,544,'chestCD',null,32,32,'chestOD',false, 70),    door1 = new Door(128,352,'doorCD',null,0,0,'doorOD'),
    door2 = new Door(416,448,'doorCL',null,0,0,'doorOL'),                    entry = new GameObject(736,64,'gateCR',null,64,64),
    switch1 = new Switch(480,128,'switchOffR',null,32,32,'switchOnR',false), plates = new PressurePlate(288,448,'plates',null,64,64,false,false),
    block1 = new GameObject(256,128,'theBlock',null,32,32),                  blocks = new GameObject(512,96,'mBlock',null,32,32),
    exit = new GameObject(0,512,'gateOL',null,64,64),                        block2 = new GameObject(480,160,'block',null,32,32),
    block3 = new GameObject(480,96,'block',null,32,32)];
    var enemy1, enemy1Alive, enemy1Text, enemy2, enemy2Alive, enemy2Text, enemy3, enemy3Alive, enemy3Text, time, delayPlayerAttack;
}

/**
 * Inherits levels.
 * @type {Levels}
 */
Level3.prototype = new Levels();

/**
 * Creates all objects for the level.
 */
Level3.prototype.create = function()
{
    for(var i = 0; i < this.levelObjects.length; i++)
    {
        if(this.levelObjects[i].image == 'gateOL')//Set so player will walk under gate.
        {
            this.player.gameElement = this.game.add.sprite(this.game.world.width - 96, 96, 'Kyros');
            this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
        }
        else if (this.levelObjects[i].image == 'theBlock')//Set so block will render on top of plates.
        {
            this.levelObjects[i].gameElement = this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
            this.game.physics.arcade.enable(this.levelObjects[i].gameElement)
        }
        else if(this.levelObjects[i].image == 'plates')//Set so plate will not move.
        {
            this.game.add.sprite(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
        }
        else
        {
            this.levelObjects[i].gameElement = this.level.create(this.levelObjects[i].x, this.levelObjects[i].y, this.levelObjects[i].image);
            if(this.levelObjects[i].image != 'mBlock')//Set so will allow block to be moved.
                this.levelObjects[i].gameElement.body.immovable = true;
            else
                this.levelObjects[i].gameElement.body.immovable = false;
        }
    }
    this.player.playerActions(this.game);
    //Create enemies for this level, health to be displayed at top of screen, boolean held to know if alive.
    enemy1 = new Enemy(160, 320, 'Grunts', null, 32, 32, 100, 60, 56, 4, 200, 4, 10, 5);
    enemy1.gameElement = this.game.add.sprite(enemy1.x, enemy1.y, enemy1.image);
    enemy1.enemyActions(this.game);
    enemy1Alive = true;
    enemy2 = new Enemy(544, 448, 'Grunts', null, 32, 32, 100, 60, 56, 4, 200, 4, 10, 5);
    enemy2.gameElement = this.game.add.sprite(enemy2.x, enemy2.y, enemy2.image);
    enemy2.enemyActions(this.game);
    enemy2Alive = true;
    enemy3 = new Enemy(304, 464, 'Grunts', null, 32, 32, 100, 60, 56, 4, 200, 4, 10, 5);
    enemy3.gameElement = this.game.add.sprite(enemy3.x, enemy3.y, enemy3.image);
    enemy3.enemyActions(this.game);
    enemy3Alive = true;
    delayPlayerAttack = new Date().getTime();
    time = new Date().getTime();
    this.goldText = this.game.add.text(5, 22, 'Gold: ' + this.player.gold, { fontSize: '32px', fill: 'gold', stroke: "black", strokeThickness: 5 });
    this.goldText.scale.setTo(0.6, 0.6);
    this.healthText = this.game.add.text(5, 5, this.player.name + "'s Health: " + this.player.health, { fontSize: '32px', fill: 'red', stroke: "black", strokeThickness: 5 });
    this.healthText.scale.setTo(0.6, 0.6);
    enemy1Text = this.game.add.text(220, 5, "Enemy1 Health: " + enemy1.health, { fontSize: '32px', fill: 'blue', stroke: "black", strokeThickness: 5 });
    enemy1Text.scale.setTo(0.6, 0.6);
    enemy2Text = this.game.add.text(400, 5, "Enemy2 Health: " + enemy2.health, { fontSize: '32px', fill: 'blue', stroke: "black", strokeThickness: 5 });
    enemy2Text.scale.setTo(0.6, 0.6);
    enemy3Text = this.game.add.text(580, 5, "Enemy3 Health: " + enemy3.health, { fontSize: '32px', fill: 'blue', stroke: "black", strokeThickness: 5 });
    enemy3Text.scale.setTo(0.6, 0.6);
}

/**
 * Update function. Sets collisions between all objects and player.
 * Resets velocity of block so will not constantly move.
 * Calls appropriate functions when player interacts with objects.
 * Checks if player interacts with enemies and takes appropriate action.
 * @param actionKey - key to be used to create actions against objects.
 */
Level3.prototype.update = function(actionKey)
{
    this.game.physics.arcade.collide(this.player.gameElement, this.level);
    this.game.physics.arcade.collide(this.level, this.level);
    this.game.physics.arcade.collide(this.player.gameElement, block1.gameElement);
    this.game.physics.arcade.collide(block1.gameElement, this.level);
    if(enemy1Alive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, enemy1.gameElement);
        this.game.physics.arcade.collide(enemy1.gameElement, this.level);
        this.game.physics.arcade.collide(enemy1.gameElement, block1.gameElement);
    }
    if(enemy2Alive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, enemy2.gameElement);
        this.game.physics.arcade.collide(enemy2.gameElement, this.level);
        this.game.physics.arcade.collide(enemy2.gameElement, block1.gameElement);
    }
    if(enemy3Alive == true)
    {
        this.game.physics.arcade.collide(this.player.gameElement, enemy3.gameElement);
        this.game.physics.arcade.collide(enemy3.gameElement, this.level);
        this.game.physics.arcade.collide(enemy3.gameElement, block1.gameElement);
    }
    blocks.gameElement.body.velocity.x = 0;   blocks.gameElement.body.velocity.y = 0;
    block1.gameElement.body.velocity.x = 0;   block1.gameElement.body.velocity.y = 0;
    block1.x = block1.gameElement.body.x;    block1.y = block1.gameElement.body.y;
    this.player.moveBlock(this.game, block1);
    this.player.moveBlock(this.game, blocks);
    if(actionKey.isDown && this.player.isTouching(switch1) == true && switch1.on == false)
        this.flickSwitchDoor(switch1, door2);
    if(actionKey.isDown && this.player.isTouching(chest1) == true && chest1.open == false)
        this.openChest(chest1, this.player);
    if(actionKey.isDown && this.player.isTouching(chest2) == true && chest2.open == false)
        this.openChest(chest2, this.player);
    if(this.isInside(block1, plates) == true)
        this.platePressed(plates, door1);
    else if(this.isInside(this.player, plates) == true)
        this.platePressed(plates, door1);
    else
        plates.pressure = false;
    if(plates.pressure == false && plates.pressed == true)
        this.plateUnPressed(plates, door1);
    if(this.isInside(this.player, exit) == true)
    {
        this.completed = true;
        this.endLevel(false);
    }
    this.player.updatePlayer(this.game);
    //Deal with enemy movements and attack, also player attack.
    if(enemy1Alive == true)
    {
        if(enemy1.inZone(this.player) == true)
            enemy1.findPlayer(this.player, this.levelObjects);
        else
            enemy1.notInZone();
        if(this.player.isTouching(enemy1) == true && time > delayPlayerAttack)
        {
            this.player.attack(enemy1, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        enemy1Text.setText('Enemy1 Health: ' + enemy1.health);
        if(enemy1.health <= 0) //enemy killed.
        {
            this.player.gold += enemy1.gold;
            enemy1.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            enemy1Alive = false;
            enemy1Text.setText('');
            this.player.isAttacking = false;
        }
    }
    if(enemy2Alive == true)
    {
        if(enemy2.inZone(this.player) == true)
            enemy2.findPlayer(this.player, this.levelObjects);
        else
            enemy2.notInZone();
        if(this.player.isTouching(enemy2) == true && time > delayPlayerAttack)
        {
            this.player.attack(enemy2, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        enemy2Text.setText('Enemy2 Health: ' + enemy2.health);
        if(enemy2.health <= 0)
        {
            this.player.gold += enemy2.gold;
            enemy2.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            enemy2Alive = false;
            enemy2Text.setText('');
            this.player.isAttacking = false;
        }
    }
    if(enemy3Alive == true)
    {
        if(enemy3.inZone(this.player) == true)
            enemy3.findPlayer(this.player, this.levelObjects);
        else
            enemy3.notInZone();
        if(this.player.isTouching(enemy3) == true && time > delayPlayerAttack)
        {
            this.player.attack(enemy3, this.game);
            delayPlayerAttack = new Date().getTime() + 500;
        }
        enemy3Text.setText('Enemy3 Health: ' + enemy3.health);
        if(enemy3.health <= 0)
        {
            this.player.gold += enemy3.gold;
            enemy3.kill();
            this.goldText.setText('Gold: ' + this.player.gold);
            enemy3Alive = false;
            enemy3Text.setText('');
            this.player.isAttacking = false;
        }
    }
    if(this.player.health <= 0)
    {
        this.player.health = 0;
        var uri = "http://mcm-highscores-hrd.appspot.com/";
        var url = uri + "score?game={0}&nickname={1}&email={2}&score={3}&func=?";
        url = url.replace('{0}', "Kyros");
        url = url.replace('{1}', this.player.name);
        url = url.replace('{2}', "oneilmike85@gmail.com");
        url = url.replace('{3}', this.player.gold);
        document.getElementById("url").innerText = url;
        $.ajax({
            type:  "GET",
            url:   url,
            async: true,
            contentType: 'application/json',
            dataType: 'jsonp',
            success: function (json)
            {
                $("#result").text(json.result);
            },
            error: function (e) {
                window.alert(e.message);
            }
        });
        this.endLevel(true);
    }
    this.healthText.setText(this.player.name + "'s Health: " + this.player.health);
    time = new Date().getTime();
}

/**
 * Deletes level if player dies or completes level. Will delete
 * player only if is dead.
 * @param playerDead - boolean to know if player is dead or alive.
 */
Level3.prototype.endLevel = function(playerDead)
{
    if(playerDead == true)
        this.player.kill();
    this.level.removeAll();
    if(enemy1Alive == true)
    {
        enemy1.kill();
        enemy1Alive = false;
    }
    if(enemy2Alive == true)
    {
        enemy2.kill();
        enemy2Alive = false;
    }
    if(enemy3Alive == true)
    {
        enemy3.kill();
        enemy3Alive = false;
    }
    delete this;
}
