/**
 *   Author: B00239553
 */

/**
 * Used to create chest.
 * @param x - position of chest.
 * @param y - position of chest
 * @param image - image for the chest when closed.
 * @param gameElement - phaser element for chest, used for collisions.
 * @param length - length of chest along x axis.
 * @param height - height of chest along y axis.
 * @param endImage - image for the chest when open.
 * @param open - boolean to know whether chest open or closed.
 * @param gold - gold value that chest holds.
 * @constructor
 */
function Chest(x, y, image, gameElement, length, height, endImage, open, gold)
{
    GameObject.apply(this, arguments);
    this.endImage = endImage;
    this.open = open;
    this.gold = gold;
}

/**
 * Inheritance from GameObject.
 * @type {GameObject}
 */
Chest.prototype = new GameObject();